import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Dimensions,
  AsyncStorage,
  Platform,
  ActivityIndicator,
  Button,
} from 'react-native';
import Modal from 'react-native-modal';
import { Icon } from 'react-native-elements';
import axios from 'axios';
import PropTypes from 'prop-types';

import { HOST } from '../services/env';

export default class ModalConfirmTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locationFrom: '',
      locatonTo: '',
      isVisible: false,
      item: [],
      id: [],
      userID: '',
    };
  }

  componentWillMount = async () => {
    try {
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);
      this.setState({ userID: parsed.id });
    } catch (error) {
      alert(error);
    }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      item: nextProps.item,
    });
  }

  toggleModal = () => {
    const { item } = this.state;
    this.setState({ isVisible: !this.state.isVisible });
  };

  // API Call to update trip to active

  onConfirm = async () => {
    try {
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);
      this.setState({ token: parsed.token, userID: parsed.id });
    } catch (error) {
      alert(error);
    }
    const { userID, token, item } = this.state;
    axios
      .put(
        `${HOST}api/users/${userID}/trips/${item._id}`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then(response => {
        console.log(response);
        this.setState({ isVisible: false });
        this.props.makeRemoteRequest();
      })
      .catch(response => {
        console.log(response);
        this.toggleModal();
      });
  };

  render() {
    const { isVisible, item, id, userID } = this.state;

    if (isVisible) {
      return (
        <View style={styles.container}>
          <Modal
            onBackdropPress={() => {
              this.toggleModal();
              //  console.warn('user id', userID);
              //  console.warn('Owners id', id);
            }}
            isVisible={isVisible}
            hideModalContentWhileAnimating
          >
            <View style={styles.modalViewContainer}>
              <View style={styles.tripInfo}>
                <View style={styles.location}>
                  <Text style={styles.textLocation}>{item.location_from}</Text>
                  <View style={styles.locationIcon}>
                    <Icon name="arrow-downward" color="#00aced" />
                  </View>
                  <Text style={styles.textLocation}>{item.location_to}</Text>
                </View>
              </View>

              <View style={styles.button}>
                <Button
                  title="Confirm"
                  color="#82d172"
                  style={{ fontSize: 18, color: 'white' }}
                  containerStyle={{
                    padding: 8,
                    marginLeft: 70,
                    marginRight: 70,
                    height: 40,
                    borderRadius: 6,
                    backgroundColor: 'mediumseagreen',
                  }}
                  onPress={this.onConfirm}
                />
              </View>
            </View>
          </Modal>
        </View>
      );
    }
    return <View />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  location: {
    flex: 1,

    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'green',
  },

  modalViewContainer: {
    height: 180,
    width: 240,
    // fix margin
    marginLeft: 36,
    backgroundColor: '#bbcbcb',
    borderColor: '#d6d7da',
    justifyContent: 'center',
    borderRadius: Platform.OS === 'ios' ? 30 : 15,
    shadowRadius: 10,
  },
  tripInfo: {
    flex: 1,
    height: 180,
    // backgroundColor: 'red',
  },
  textLocation: {
    fontSize: 24,
    fontWeight: '700',
  },

  title: {
    justifyContent: 'center',
    fontSize: 16,
    marginLeft: 8,
    fontWeight: 'bold',
    top: 10,

    shadowRadius: 8,
  },
  button: {
    // backgroundColor: 'green',
    paddingBottom: 12,
    width: 120,
    alignSelf: 'center',
  },
});

const { width, height } = Dimensions.get('window');
const CONTAINER_WIDTH = 0.9;
const Modal_HEIGHT = height / 6;
