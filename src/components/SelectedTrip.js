import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class SelectedTrip extends Component {
  static navigationOptions = {
    title: 'Confirm Trip',
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <Text> {this.props.navigation.state.params.trip}        </Text>
      </View>
    );
  }
}
