import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  AsyncStorage,
  Button,
} from 'react-native';
import { Divider, Card, Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import {
  apiTripFetchSingle,
  apiTripFinish,
  apiTripTotalFinish,
} from '../services/api';
import { HOST } from '../services/env';

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

export default class TripFinish extends Component {
  state = {
    visibleModal: null,
    newTrip: [],
    timer: null,
    token: '',
    id: '',
    loading: true,
  };

  componentDidMount() {
    this.makeRequest();
  }

  // come back to
  checkUser() {
    const { id } = this.state;
    const { trip } = this.state;
    if (id === trip.owner.id) {
      return trip.owner.name;
    }
    return trip.owner.name;
  }

  tripCheck() {
    console.log('checking token', id, token);
    const { newTrip, id, token } = this.state;
    console.log('tripCheck', newTrip);
    const userComplete = newTrip.owner.completed;
    const ownerComplete = newTrip.user_confirm.completed;
    if (userComplete && ownerComplete) {
      // another post to actually "end the trip"
      apiTripTotalFinish(newTrip, token, id);
      console.log('Success two user completed');
      this.setState({ loading: false });
    } else {
      console.log('waiting for user to complete their trip');
    }
  }

  async makeRequest() {
    const { trip } = this.props.navigation.state.params;
    console.log(trip);

    try {
      // refactor - use in fetch - change to await fetch
      // also change time
      await wait(10000);
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);

      this.setState({ token: parsed.token, id: parsed.id });
    } catch (error) {
      alert(error);
    }
    const { token, id } = this.state;

    const url = `${HOST}api/users/${id}/trips/${trip._id}`;

    this.setState({ loading: true });
    fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          newTrip: resJson,
          error: resJson.error || null,
        });

        this.tripCheck();
      })
      .catch(error => {
        alert('Error occured when fetchin data!');
        console.log(error);
      });
  }

  render() {
    const {
      trip,
      distanceTravelled,
      tripScore,
    } = this.props.navigation.state.params;
    const distanceRounded = Math.round(distanceTravelled);
    return (
      <View>
        <View style={styles.container}>
          <Card featuredTitleStyle={styles.cardTitle} title="Trip Complete">
            <View style={styles.cardContainer}>
              <View style={styles.textContainer}>
                <View style={styles.infoContainers}>
                  <View style={styles.info1}>
                    <Text style={styles.textInfo}>{`+ ${tripScore}`}</Text>
                    <Text style={styles.textInfoSub}>Points</Text>
                  </View>

                  <View style={styles.info3}>
                    <Text style={styles.textInfo}>
                      {`${distanceRounded} km`}
                    </Text>
                    <Text style={{ fontSize: 20, fontWeight: '400' }}>
                      {' '}
                      Travelled
                    </Text>
                  </View>
                </View>
                <View style={styles.divider}>
                  <Divider
                    style={{
                      backgroundColor: '#f5f5f0',
                    }}
                  />
                </View>
                <View style={styles.divider}>
                  <Divider
                    style={{
                      backgroundColor: '#f5f5f0',
                      marginTop: 16,
                    }}
                  />
                </View>
                <View style={styles.bottomContainer}>
                  <View style={styles.bottomBarContent}>
                    <Text style={styles.bottomText}>
                      {' '}
                      Waiting on user to finish{' '}
                    </Text>

                    {this.state.loading && <ActivityIndicator />}
                    {!this.state.loading && (
                      <View>
                        <Text style={styles.successText}> Success ! </Text>
                        <Icon name="check-circle" size={32} />
                      </View>
                    )}
                  </View>

                  <Button
                    color="#82d172"
                    title="Return"
                    onPress={() => this.props.navigation.popToTop()}
                  />
                </View>
              </View>
            </View>
          </Card>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    //  backgroundColor: 'black',
  },

  textContainer: {
    display: 'flex',
    //  backgroundColor: '#e5ffde',
    borderRadius: 10,
    height: 360,
  },
  infoContainers: {
    height: 180,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  bottomContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomBarContent: {
    justifyContent: 'space-between',
  },
  cardTitle: {
    fontSize: 24,
  },
  bottomText: {
    fontSize: 24,
    fontWeight: '700',
  },
  divider: {},
  info1: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: 100,
    height: 100,
    backgroundColor: '#82d172',
    borderRadius: 24,
  },
  info2: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: 100,
    height: 100,
    backgroundColor: '#82d172',
    borderRadius: 24,
  },
  info3: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: 100,
    height: 100,
    backgroundColor: '#82d172',
    borderRadius: 24,
  },
  textInfo: {
    fontSize: 26,
    fontWeight: '700',
    textShadowRadius: 4,
    textShadowColor: '#f5f5f0',
    textShadowOffset: { width: 2, height: 2 },
  },
  textInfoSub: {
    fontSize: 20,
    fontWeight: '400',
  },
  textPre: {
    marginTop: 10,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  textPoints: {
    marginTop: 10,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    fontSize: 32,
  },
  successText: {
    fontSize: 32,
    alignSelf: 'center',
    fontWeight: '700',
  },
  paddingButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 80,
    height: 380,
    borderRadius: 40,
    alignSelf: 'center',
    marginBottom: 20,
  },
});
