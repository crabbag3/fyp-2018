import React, { Component } from 'react';
import {
  View,
  Text,
  AsyncStorage,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
  Alert,
} from 'react-native';
import {
  List,
  ListItem,
  SearchBar,
  Button,
  Header,
  Image,
  Icon,
} from 'react-native-elements';
import { HOST } from '../services/env';
import { apiFetchUsers, userData } from '../services/api';

export default class LeaderBoards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      token: '',
      refreshing: false,
      loading: false,
      item: null,

      error: null,
    };
  }

  componentDidMount() {
    this.fetchUsers();
  }

  handleRefresh = () => {
    this.fetchUsers();
  };

  fetchUsers = async () => {
    // fetch user data
    try {
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);
      this.setState({ token: parsed.token, id: parsed.id });
    } catch (error) {
      alert(error);
    }
    const { page, token, id } = this.state;
    // fetch users that have points
    const url = `${HOST}api/users/`;
    this.setState({ loading: true });
    fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          data: resJson.user,
          loading: false,
        });
        this.arrayholder = resJson.user;
        console.log(this.state.data);
      })
      .catch(error => {
        Alert.alert('Error occured when fetchin data!');
        console.log(error);
        this.setState({ error, loading: false, refreshing: false });
      });
  };

  // come back to - dont think does anything
  renderSeparator = () => (
    <View
      style={{
        height: 1,
        width: '86%',
        backgroundColor: '#CED0CE',
        marginLeft: '14%',
      }}
    />
  );

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    const { data } = this.state;
    if (this.state.loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <List
        containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0, flex: 1 }}
      >
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <ListItem
              title={`${item.name}`}
              titleStyle={{ fontSize: 18 }}
              subtitle={`${item.completedTripCount} Trips`}
              subtitleStyle={{ fontSize: 14 }}
              rightTitle={`${Math.round(item.points)} Points`}
              rightTitleStyle={{ fontSize: 16, fontWeight: '700' }}
              containerStyle={{ height: 80 }}
              avatar={<Icon name="person" size={36} />}
              rightIcon={{ name: 'stars' }}
            />
          )}
          keyExtractor={item => item._id}
          ItemSeparatorComponent={this.renderSeparator}
          // ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />
      </List>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: height / 2,
  },
});
const { width, height } = Dimensions.get('window');
