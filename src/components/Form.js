import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import validation from './validators/validation';
import validate from './validators/validate_wrapper';
import { HOST } from '../services/env';

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      emailError: '',
      name: '',
      password: '',
      passwordError: '',
      authenticating: false,
      errorSignup: false,
    };
    this.login = this.login.bind(this);
  }

  // On Login/Singup - validate forms then submit
  buttonPress() {
    const emailError = validate('email', this.state.email);
    const passwordError = validate('password', this.state.password);
    this.setState({
      emailError,
      passwordError,
    });

    if (!emailError && !passwordError) {
      this.login();
    }
  }

  // Based off of prop type dtermines to use sign in/log in method
  async login() {
    const { type, navigation } = this.props;
    const { email, password, name } = this.state;
    this.setState({ authenticating: true });
    if (type === 'Signup') {
      axios
        .post(`${HOST}api/auth/signup`, {
          email,
          password,
          name,
        })
        .then(response => {
          const userData = response.data;

          AsyncStorage.setItem('userData', JSON.stringify(userData));
          this.setState({ authenticating: false });
          navigation.navigate('SignedIn');
        })
        .catch(response => {
          this.setState({ authenticating: false, errorSignup: true });
          const error = response;
          console.log(error);
        });
    } else {
      axios
        .post(`${HOST}api/auth/signin`, {
          email,
          password,
        })
        .then(response => {
          const userData = response.data;
          AsyncStorage.setItem('userData', JSON.stringify(userData));
          this.setState({ authenticating: false });
          this.props.navigation.navigate('SignedIn');
        })
        .catch(error => {
          console.log(error);
          this.setState({ authenticating: false, errorLogin: true });
        });
    }
  }

  render() {
    // dont use destructering here
    if (this.state.authenticating) {
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    const { email, name, password } = this.state;
    const { type } = this.props;
    return (
      <View style={styles.container}>
        {type === 'Signup' && (
          <TextInput
            style={styles.inputBox}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Name"
            name="name"
            placeholderTextColor="#ffffff"
            onSubmitEditing={() => {
              this.email.focus();
            }}
            onChangeText={name => this.setState({ name })}
            value={name}
            blurOnSubmit={false}

            // fix
          />
        )}
        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="rgba(0,0,0,0)"
          placeholder="Email"
          placeholderOpacity
          name="email"
          placeholderTextColor="#ffffff"
          onSubmitEditing={() => this.password.focus()}
          onChangeText={email => this.setState({ email })}
          value={email}
          blurOnSubmit={false}
          autoCapitalize="none"
          onBlur={() => {
            this.setState({
              emailError: validate('email', this.state.email),
            });
          }}
        />
        {/* Form Validation */}
        <Text style={styles.errorText}>{this.state.emailError}</Text>
        <TextInput
          style={styles.inputBox}
          underlineColorAndroid="rgba(0,0,0,0)"
          secureTextEntry
          placeholder="Password"
          name="password"
          placeholderTextColor="#ffffff"
          value={password}
          onBlur={() => {
            this.setState({
              passwordError: validate('password', this.state.password),
            });
          }}
          onChangeText={password => this.setState({ password })}
          ref={input => (this.password = input)}
          autoCapitalize="none"
        />
        {/* Form Validation */}
        <Text style={styles.errorText}>{this.state.passwordError}</Text>

        <TouchableOpacity
          onPress={() => this.buttonPress()}
          style={styles.button}
        >
          <Text style={styles.buttonText}>{this.props.type}</Text>
        </TouchableOpacity>
        {/* Error handling server side - refactor */}
        {this.state.errorSignup && (
          <Text style={styles.errorText}>
            Oops that email is already registered
          </Text>
        )}
        {this.state.errorLogin && (
          <Text style={styles.errorText}>Oops invalid email/password</Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container1: {
    flex: 1,
    justifyContent: 'center',
  },
  container: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputBox: {
    width: 310,
    backgroundColor: 'rgba(255,2555,255,0.3)',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10,
  },
  button: {
    width: 300,
    backgroundColor: '#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  errorText: {
    color: 'red',
    fontSize: 16,
  },
});
