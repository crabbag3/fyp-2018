import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  Image,
  AsyncStorage,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {
  List,
  ListItem,
  SearchBar,
  Button,
  Header,
} from 'react-native-elements';
import { icons } from 'react-native-vector-icons';
import moment from 'moment';
import { HOST } from '../services/env';
import { apiTripFetch } from '../services/api';

export default class PastTrips extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      error: null,
      page: 1,
      refreshing: false,
      token: '',
      item: null,
      dateISO: '',
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = async () => {
    try {
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);
      this.setState({ token: parsed.token, id: parsed.id });
    } catch (error) {
      alert(error);
    }
    const { page, token, id } = this.state;

    const url = `${HOST}api/users/${id}/trips/trips-complete`;
    this.setState({ loading: true });
    fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          data: resJson,
          error: resJson.error || null,
          loading: false,
        });
        this.arrayholder = resJson;
      })
      .catch(error => {
        alert('Error occured when fetchin data!');
        this.setState({ error, loading: false, refreshing: false });
      });
  };

  // come back to - dont think does anything
  renderSeparator = () => (
    <View
      style={{
        height: 1,
        width: '86%',
        backgroundColor: '#CED0CE',
        marginLeft: '14%',
      }}
    />
  );

  // Runs a filter function on the arrayholder (which contains our fetched data)
  searchFilterFunction = text => {
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.location_to.toUpperCase()}   
			${item.location_from.toUpperCase()} ${item.location_to.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({ data: newData });
  };

  renderHeader = () => <Text>Your Past Trips</Text>;

  renderFooter = () => {
    const { loading } = this.state;
    if (!this.state.loading) return null;
    return (
      <View
        stle={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderTopColor: '#CED0CE',
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  handleRefresh = () => {
    this.makeRemoteRequest();
  };

  toggleDisplayTrip = item => {
    alert('Find out more info about your trip');
  };

  // Sets text size dynamcially
  renderTitleStyle = item => {
    const infoText = item.location_from + item.location_to;
    if (infoText.length) {
      return { fontSize: infoText.length > 14 ? 12 : 16 };
    }
    console.warn('param item has no props');
  };

  renderPointsStyle = item => {
    const distance = Math.round(item.distanceTravelled);
    return (
      <Text>
        {distance}
        km
      </Text>
    );
  };

  // ListItem render
  renderItem(item) {
    const dateISO = item.dateISO;
    const countDown = moment(dateISO).fromNow();
    const distance = Math.round(item.distanceTravelled);
    return (
      /* eslint-disable */
      <ListItem
        style={styles.listTrip}
        roundAvatar
        titleStyle={this.renderTitleStyle(item)}
        containerStyle={styles.listItem}
        title={`${item.location_from} to ${item.location_to} `}
        subtitle={
          <View style={styles.subtitle}>
            <Text>{item.timeStamp}</Text>
            <Text> + {Math.round(item.tripScore)} points</Text>
						<Text style={{fontWeight: '800'}}> {item.owner.name}</Text>
          </View>
        }
        rightTitle={`${distance} km`}
				rightTitleStyle={{ fontWeight: 'bold' }}
				/* eslint-enable */
        avatar={
          <Image
            source={require('../assests/carLogo.png')}
            style={{ width: 40, height: 62, borderRadius: 10 }}
          />
        }
      />
    );
  }

  // Pass the item of the selected listitem to the modal

  render() {
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            paddingTop: height / 3,
          }}
        >
          <ActivityIndicator size="large" />
        </View>
      );
    }

    const { data, refreshing, loading, item } = this.state;

    return (
      <View style={styles.container}>
        <List
          containerStyle={{
            borderTopWidth: 0,
            borderBottomWidth: 0,
            display: 'flex',
          }}
        >
          <FlatList
            data={data}
            renderItem={({ item }) => this.renderItem(item)}
            // Uses object ID to iterate over the trips
            keyExtractor={item => item._id}
            ItemSeparatorComponent={this.renderSeparator}
            ListFooterComponent={this.renderFooter}
            refreshing={refreshing}
            onRefresh={this.handleRefresh}
          />
        </List>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contaidener: {
    flex: 1,
    height: 400,
  },
  listItem: {
    flex: 1,
    justifyContent: 'center',
    height: 120,
  },
  subtitle: {
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 10,
  },
  button: {
    display: 'flex',
    position: 'absolute',
    bottom: 0,
    paddingBottom: 5,
    // must change the padding style left
    paddingLeft: 175,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 50,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const { width, height } = Dimensions.get('window');
