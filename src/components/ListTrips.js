import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  Image,
  AsyncStorage,
  StyleSheet,
  Dimensions,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {
  List,
  ListItem,
  SearchBar,
  Button,
  Header,
} from 'react-native-elements';
import { icons } from 'react-native-vector-icons';
import moment from 'moment';
import Swipeout from 'react-native-swipeout';
import axios from 'axios';
import { HOST } from '../services/env';
import ModalAddTrip from './ModalAddTrip';
import ModalConfirmTrip from './ModalConfirmTrip';

export default class ListTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      error: null,
      page: 1,
      refreshing: false,
      token: '',
      item: null,
      date: null,
      activeTrip: null,
      swipeState: null,
      coords: [],
    };
  }

  componentWillMount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  componentDidMount() {
    this.makeRemoteRequest();
    // Location needed for adding a trip - search nearby results
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({ coords: position.coords });
        console.log('Starting', this.state.coords);
      },
      error => Alert.alert('Unable to locate position'),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
      },
    );
  }

  makeRemoteRequest = async () => {
    // fetch user data
    try {
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);
      this.setState({ token: parsed.token, id: parsed.id });
    } catch (error) {
      alert(error);
    }
    const { page, token, id } = this.state;
    // fetch non-active trips
    const url = `${HOST}api/users/${id}/trips/not-active`;
    this.setState({ loading: true });
    fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          data: resJson,
          loading: false,
        });
        this.arrayholder = resJson;
        console.log(this.state.data);
      })
      .catch(error => {
        Alert.alert('Error occured when fetchin data!');
        console.log(error);
        this.setState({ error, loading: false, refreshing: false });
      });
  };

  // delete a trip
  deleteTrip = async item => {
    const { id, token } = this.state;

    axios
      .delete(`${HOST}api/users/${id}/trips/${item._id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(response => {
        Alert.alert('You deleted a trip');
        console.log(response);
        // reload the page on success
        this.makeRemoteRequest();
      })
      .catch(response => {
        Alert.alert('Trip deletion failed');
        console.log(response);
      });
  };

  // not using - come back to
  renderSeparator = () => (
    <View
      style={{
        height: 1,
        width: '86%',
        backgroundColor: '#CED0CE',
        marginLeft: '14%',
      }}
    />
  );

  // Runs a filter function on the arrayholder (which contains our fetched data)
  searchFilterFunction = text => {
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.location_to.toUpperCase()}   
			${item.location_from.toUpperCase()} ${item.location_to.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({ data: newData });
  };

  renderHeader = () => (
    <SearchBar
      placeholder="Search for a ride"
      lightTheme
      round
      onChangeText={text => this.searchFilterFunction(text)}
      autoCorrect={false}
    />
  );

  renderBackgroundColor = () => <View style={{ backgroundColor: '#455a64' }} />;

  // currently not using - come back to
  renderFooter = () => {
    const { loading } = this.state;
    if (!loading) return null;
    return (
      <View
        stle={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderTopColor: '#CED0CE',
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  /* renderDate = () => {
    this.setState({ date: this.arrayholder.date });
    const date = moment().format('MMMM Do h:mm a');
    return { date };
  };
	*/
  handleRefresh = () => {
    this.makeRemoteRequest();
  };

  // Pass the item of the selected listitem to the modal
  toggleModalConfirmTrip = item => {
    this.setState({ item });
    const { id } = this.state;

    // parsing date - don't allow user to accept past trips
    const countDown = moment(item.dateISO).fromNow();
    const pastTime = 'ago';

    // trip ID's are same as the user logged in and the trips owner
    if (id === item.user[0]._id) {
      Alert.alert(
        'Sorry you cannot accept your own trip!\nSwipe right to delete your trip',
      );
    } else if (countDown.includes(pastTime)) {
      Alert.alert('Sorry you cannot accept past trips!');
    } else if (this.ModalConfirmTrip) {
      this.ModalConfirmTrip.toggleModal();
    }
  };

  // Add Trip Modal
  toggleModalAddTrip = () => {
    if (this.ModalAddTrip) {
      this.ModalAddTrip.toggleModal();
    }
  };

  // Sets text size dynamically
  renderTitleStyle = item => {
    const infoText = item.location_from + item.location_to;
    if (infoText.length) {
      return { fontSize: infoText.length > 14 ? 12 : 16 };
    }
  };

  // Sets color of of trip to green if user owner
  renderItemContainerStyle = item => {
    const { id } = this.state;
    if (id === item.user[0]._id) {
      return {
        backgroundColor: '#e5ffde',
        borderBottomWidth: 0,
        paddingBottom: 10,
      };
    }
    return {
      borderBottomWidth: 0,
      paddingBottom: 10,
    };
  };

  // ListItem render delete button
  renderItem(item) {
    console.log('item list');
    const { id } = this.state;
    const swipeBtns = [
      {
        text: 'Delete',
        backgroundColor: 'red',
        underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
        onPress: () => {
          this.deleteTrip(item);
        },
      },
    ];

    return (
      <Swipeout
        right={swipeBtns}
        autoClose
        backgroundColor="transparent"
        disabled={id !== item.user[0]._id}
      >
        <ListItem
          style={styles.activeUser}
          onPress={() => this.toggleModalConfirmTrip(item)}
          roundAvatar
          subtitle={item.user[0].name}
          titleStyle={this.renderTitleStyle(item)}
          title={`${item.location_from} to ${item.location_to} `}
          rightTitle={item.timeStamp}
          avatar={
            <Image
              source={require('../assests/carLogo.png')}
              style={{ width: 40, height: 50, borderRadius: 10 }}
            />
          }
          // conditonally dispalying a light green background if the user created the trip
          containerStyle={this.renderItemContainerStyle(item)}
        />
      </Swipeout>
    );
  }

  // Delete Modal

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator size="large" />
        </View>
      );
    }

    const { data, refreshing, loading, item, date, id } = this.state;

    return (
      <View style={styles.container}>
        <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
          <FlatList
            data={data}
            renderItem={({ item }) => this.renderItem(item)}
            // Uses object ID to iterate over the trips
            keyExtractor={item => item._id}
            ItemSeparatorComponent={this.renderSeparator}
            ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            refreshing={refreshing}
            onRefresh={this.handleRefresh}
          />
        </List>
        <View style={styles.button}>
          <Button
            onPress={this.toggleModalAddTrip}
            title="+"
            color="#D8F793"
            backgroundColor="#82d172"
            titleStyle={{ fontWeight: '900' }}
            buttonStyle={{
              borderRadius: 6,
              width: width / 8,
            }}
          />
        </View>
        <ModalAddTrip
          ref={ref => {
            this.ModalAddTrip = ref;
          }}
          makeRemoteRequest={this.makeRemoteRequest}
          coords={this.state.coords}
        />
        <ModalConfirmTrip
          ref={ref => {
            this.ModalConfirmTrip = ref;
          }}
          makeRemoteRequest={this.makeRemoteRequest}
          item={item}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    display: 'flex',
    position: 'absolute',
    bottom: 0,
    paddingBottom: 5,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: height / 2,
  },
  activeUser: {
    /*  flex: 1,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 50,
    bottom: 0,
  */ alignItems:
      'center',
    justifyContent: 'center',
  },
});

const { width, height } = Dimensions.get('window');
