import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  Image,
  AsyncStorage,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {
  List,
  ListItem,
  SearchBar,
  Button,
  Header,
} from 'react-native-elements';
import { icons } from 'react-native-vector-icons';
import moment from 'moment';
import { HOST } from '../services/env';
import ModalAddTrip from './ModalAddTrip';
import ModalConfirmTrip from './ModalConfirmTrip';

export default class ListTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      error: null,
      page: 1,
      refreshing: false,
      token: '',
      item: null,
      dateISO: '',
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = async () => {
    try {
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);
      this.setState({ token: parsed.token, id: parsed.id });
    } catch (error) {
      alert(error);
    }
    const { page, token, id } = this.state;
    const url = `${HOST}api/users/${id}/trips/active`;
    this.setState({ loading: true });
    fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          data: resJson,
          error: resJson.error || null,
          loading: false,
        });
        this.arrayholder = resJson;
      })
      .catch(error => {
        alert('Error occured when fetchin data!');
        this.setState({ error, loading: false, refreshing: false });
      });
  };

  renderSeparator = () => (
    <View
      style={{
        height: 1,
        width: '86%',
        backgroundColor: '#CED0CE',
        marginLeft: '14%',
      }}
    />
  );

  // Runs a filter function on the arrayholder (which contains our fetched data)
  searchFilterFunction = text => {
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.location_to.toUpperCase()}   
			${item.location_from.toUpperCase()} ${item.location_to.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({ data: newData });
  };

  renderHeader = () => <Text>Your Trips</Text>;

  renderFooter = () => {
    const { loading } = this.state;
    if (!this.state.loading) return null;
    return (
      <View
        stle={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderTopColor: '#CED0CE',
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  handleRefresh = () => {
    this.makeRemoteRequest();
  };

  toggleDisplayTrip = item => {
    alert('Find out more info about your trip');
  };

  // Find out more info about trip page
  handleTripView(item) {
    const { navigation } = this.props;
    this.props.navigation.push('TripView');
  }

  // Sets text size dynamcially
  renderTitleStyle = item => {
    const infoText = item.location_from + item.location_to;
    if (infoText.length) {
      return { fontSize: infoText.length > 14 ? 12 : 16 };
    }
    console.warn('param item has no props');
  };

  renderItemStyle = item => {
    const dateISO = item.dateISO;
    const countDown = moment(dateISO).fromNow();
    const pastTime = 'ago';
    const nearTime = 'minutes';
    const futureTime = 'in';
    if (countDown.includes(pastTime)) {
      return {
        backgroundColor: '#e6e6e6',
        borderBottomWidth: 0,
        paddingBottom: 10,
      };
    }
    if (countDown.includes(nearTime)) {
      return {
        backgroundColor: 'orange',
        borderBottomWidth: 0,
        paddingBottom: 10,
      };
    }
    if (countDown.includes(futureTime)) {
      return {
        backgroundColor: '#66ff33',
        borderBottomWidth: 0,
        paddingBottom: 10,
      };
    }
  };

  // ListItem render
  renderItem(item) {
    const dateISO = item.dateISO;
    const countDown = moment(dateISO).fromNow();

    return (
      <ListItem
        onPress={() => this.props.navigation.push('TripView', { item })}
        roundAvatar
        titleStyle={this.renderTitleStyle(item)}
        title={`${item.location_from} to ${item.location_to} `}
        // subtitle={item.user[0].name}
        rightTitle={item.timeStamp}
        avatar={
          <Image
            source={require('../assests/carLogo.png')}
            style={{ width: 40, height: 50, borderRadius: 10 }}
          />
        }
        containerStyle={this.renderItemStyle(item)}
      />
    );
  }

  // Pass the item of the selected listitem to the modal

  render() {
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            paddingTop: height / 3,
          }}
        >
          <ActivityIndicator size="large" />
        </View>
      );
    }

    const { data, refreshing, loading, item } = this.state;

    return (
      <View style={styles.container}>
        <List>
          <FlatList
            data={data}
            renderItem={({ item }) => this.renderItem(item)}
            // Uses object ID to iterate over the trips
            keyExtractor={item => item._id}
            ItemSeparatorComponent={this.renderSeparator}
            ListFooterComponent={this.renderFooter}
            refreshing={refreshing}
            onRefresh={this.handleRefresh}
          />
        </List>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    display: 'flex',
    position: 'absolute',
    bottom: 0,
    paddingBottom: 5,
    // must change the padding style left
    paddingLeft: 175,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 50,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const { width, height } = Dimensions.get('window');
