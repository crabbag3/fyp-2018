import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

export default class Logo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{ width: 80, height: 100, borderRadius: 10 }}
          source={require('../assests/carLogo.png')}
        />
        <Text style={styles.logoText}>Welcome to CarShare</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginHorizontal: 40,
  },
  logoText: {
    marginVertical: 15,
    fontSize: 18,
    color: 'rgba(255, 255, 255, 0.7)',
  },
});
