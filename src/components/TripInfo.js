import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  Image,
  Button,
} from 'react-native';
import { Overlay } from 'react-native-elements';

import MapView, { Marker, AnimatedRegion, Polyline } from 'react-native-maps';
import haversine from 'haversine';
import moment from 'moment';
import axios from 'axios';
import LoadingTripScreen from './LoadingTripScreen';
import { HOST, apiKey } from '../services/env';
import { handleDirections, handleTripCoords } from '../services/apiMaps';
import { apiTripFinish } from '../services/api';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 29.95539;
const LONGITUDE = 78.07513;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class TripInfo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: true,
      watching: false,
      loading: true,
      loadingPre: true,
      locationToCoords: [],
      locationFromCoords: [],
      latitude: LATITUDE,
      longitude: LONGITUDE,
      destinationCoordinates: new AnimatedRegion({
        latitude: 38.422,
        longitude: -122.084,
      }),
      routeCoordinates: [],
      distanceTravelled: 0,
      prevLatLng: {},
      coordinate: new AnimatedRegion({
        latitude: LATITUDE,
        longitude: LONGITUDE,
      }),
      coords: [],
      isTimeBetween: '',
    };
  }

  /* getInitialState() {
    return {
      region: {
        latitude: this.state.locationFromCoords.lat,
        longitude: this.state.locationFromCoords.lng,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
  } */

  componentWillMount() {
    // console.log(trip);
    navigator.geolocation.getCurrentPosition(
      position => {
        console.log('Starting', position);
      },
      error => alert(error.message),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
      },
    );
  }

  componentDidMount() {
    this.tripCords();

    this.watchID = navigator.geolocation.watchPosition(
      position => {
        const {
          coordinate,
          routeCoordinates,
          distanceTravelled,
          watching,
        } = this.state;
        const { latitude, longitude } = position.coords;
        console.log('Inital cords', this.state.latitude, this.state.longitude);
        // pass position coords into tripCheck

        const newCoordinate = {
          latitude,
          longitude,
        };
        // waits for current position to render view

        if (Platform.OS === 'android') {
          if (this.marker) {
            this.marker._component.animateMarkerToCoordinate(
              newCoordinate,
              500,
            );
          }
        } else {
          coordinate.timing(newCoordinate).start();
        }

        this.setState({
          loadingPre: false,
          latitude,
          longitude,
          routeCoordinates: routeCoordinates.concat([newCoordinate]),
          distanceTravelled:
            distanceTravelled + this.calcDistance(newCoordinate),
          prevLatLng: newCoordinate,
        });
        // console.log('watching new cord', this.state.newCoordinate);
        // console.log('prev cord', this.state.prevLatLng);
        console.log(this.state.watching);
        if (this.state.watching) {
          this.tripCheck(position.coords);
        }
      },
      error => console.log(error),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  // calculates the distances travelled - uses haversine formula
  calcDistance = newLatLng => {
    const { prevLatLng } = this.state;
    return haversine(prevLatLng, newLatLng) || 0;
  };

  getMapRegion = () => ({
    latitude: this.state.latitude,
    longitude: this.state.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });

  async tripCheck(currentCords) {
    const {
      locationFromCoords,
      locationToCoords,
      distanceTravelled,
    } = this.state;
    const trip = this.props.trip;
    const { latitude, longitude } = currentCords;
    const endCoords = {
      latitude: locationToCoords.lat,
      longitude: locationToCoords.lng,
    };
    const startCoords = {
      latitude: locationFromCoords.lat,
      longitude: locationFromCoords.lng,
    };
    console.log('Start coords', startCoords);
    console.log('Current coords', latitude, longitude);
    console.log('Destination', locationToCoords);

    // checks if trip is soon and they are near the  starting location;
    const distanceStart = Math.round(haversine(currentCords, startCoords));
    console.log('distance away from start', distanceStart);

    console.log('start watching');
    // currently doesnt affect state this.setState({ watching: true });

    // calculates distances between current position and destination
    const distanceAway = Math.round(haversine(currentCords, endCoords));
    console.log('Distance away', distanceAway);

    // checks if user is near the destination loaction
    if (distanceAway <= 1) {
      this.setState({ watching: false });

      // calculate trip score
      const tripScore = Math.round(distanceTravelled * 0.5) + 20;
      Math.round(distanceTravelled);
      console.log('score', tripScore);
      console.log('Pre post trip complete');
      /* this.props.navigation.navigate('TripFinish', {
          distanceTravelled,
          trip,
          tripScore,
        }); */

      // post trip score to API
      try {
        // get currentUser info
        const user = await AsyncStorage.getItem('userData');
        const parsed = JSON.parse(user);

        const token = parsed.token;
        const id = parsed.id;

        // better way to do this come back to
        if (id === trip.owner.id && trip.user_confirm.completed === true) {
          console.log('id is', id, 'owner id is', trip.owner.id);
          const { ownerTrue, userTrue } = true;
          apiTripFinish(
            tripScore,
            distanceTravelled,
            trip,
            token,
            id,
            true,
            true,
          );
        } else if (
          id === trip.user_confirm.id &&
          trip.owner.completed === true
        ) {
          const { ownerTrue, userTrue } = true;
          apiTripFinish(
            tripScore,
            distanceTravelled,
            trip,
            token,
            id,
            true,
            true,
          );
        } else if (id === trip.owner.id) {
          const ownerTrue = true;
          apiTripFinish(
            tripScore,
            distanceTravelled,
            trip,
            token,
            id,
            true,
            false,
          );
        } else {
          const userTrue = true;
          apiTripFinish(
            tripScore,
            distanceTravelled,
            trip,
            token,
            id,
            false,
            true,
          );
        }

        this.props.navigation.navigate('TripFinish', {
          trip,
          distanceTravelled,
          tripScore,
        });
        console.log('Prelim post success');
        // stop watching location
        navigator.geolocation.clearWatch(this.watchID);
      } catch (error) {
        alert(error);
      }
    } else console.log('Not finished');
    // setTimeout(this.tripCheck(currentCords), 1000);
  }

  // fetch coords of trip info via maps API
  async tripCords() {
    // trip is passed as prop from TripView(via onPress list item)
    const { trip } = this.props;
    try {
      // Use PlaceID Google API to fetch coordinates of origin and destination
      handleTripCoords(trip.location_from_id).then(response => {
        console.log('response is from', response);
        this.setState({ locationFromCoords: response });
      });

      handleTripCoords(trip.location_to_id).then(response => {
        console.log('response is to', response);
        this.setState({ locationToCoords: response });
      });
    } catch (err) {
      console.log(err);
    }
  }

  // onPress start Trip - trip can only start 15 mins before/after
  async handleTripStart() {
    const { trip } = this.props;
    const dateISO = trip.dateISO;
    const countDown = moment(dateISO).fromNow();
    const beforeNow = moment().subtract(15, 'm');
    const afterNow = moment().add(15, 'm');
    const isTimeBetween = moment(dateISO).isBetween(beforeNow, afterNow);
    console.log('handle button', isTimeBetween);
    if (isTimeBetween) {
      this.setState({ loading: false, watching: true });
      console.log('Start watching button press');
    } else alert('You can only start your trip closer to the time!');
  }

  // calls MapsAPI to fetch directions on button press
  async directionButton() {
    handleDirections(
      [this.state.locationToCoords.lat, this.state.locationToCoords.lng],
      [this.state.locationFromCoords.lat, this.state.locationFromCoords.lng],
    ).then(response => {
      this.setState({ coords: response });
      this.map.fitToCoordinates(response, {
        edgePadding: { left: 0, right: 0, top: 0, bottom: 0 },
      });
    });
  }

  // api call to plot directions
  /*  handleDirections(
      [this.state.locationToCoords.lat, this.state.locationToCoords.lng],
      [this.state.locationFromCoords.lat, this.state.locationFromCoords.lng],
    ).then(response => {
      this.setState({ coords: response });
    });
  }  */

  render() {
    const {
      locationFromCoords,
      locationToCoords,
      loading,
      distanceTravelled,
      tripScore,
      loadingPre,
    } = this.state;
    const trip = this.props.trip;
    const dateISO = trip.dateISO;
    const countDown = moment(dateISO).fromNow();
    // loading screen, multiple API calls so slow (seperate component)
    if (this.state.loading) {
      return (
        <View style={{ flex: 1 }}>
          <LoadingTripScreen
            trip={trip}
            locationFromCoords={locationFromCoords}
            locationToCoords={locationToCoords}
            loading={loadingPre}
          />
          <Button
            title="Start Trip"
            color="#82d172"
            onPress={() => this.handleTripStart()}
          />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <MapView
          ref={map => {
            this.map = map;
          }}
          style={styles.map}
          showsUserLocation
          followUserLocation
          loadingEnabled
          region={this.getMapRegion()}
        >
          <Polyline
            coordinates={this.state.routeCoordinates}
            strokeWidth={5}
            strokeColor="#19B5FE"
          />
          <Polyline
            coordinates={this.state.coords}
            strokeWidth={5}
            strokeColor="green"
          />
          <MapView.Marker
            coordinate={{
              latitude: locationToCoords.lat,
              longitude: locationToCoords.lng,
            }}
          />
          <MapView.Marker
            coordinate={{
              latitude: locationFromCoords.lat,
              longitude: locationFromCoords.lng,
            }}
          />
        </MapView>
        <View style={styles.navBar}>
          <Text style={styles.navBarText}>
            Driver: 
{' '}
{trip.owner.name} 
{' '}
{'\n'} {trip.location_from} to{' '}
            {trip.location_to}
          </Text>
        </View>
        <View style={styles.buttonDirection}>
          <Button
            title="Directions"
            color="#82d172"
            onPress={() => this.directionButton()}
          />
        </View>
        <View style={styles.bottomBar}>
          <View style={styles.bottomBarGroup}>
            <Text style={styles.bottomBarHeader}>DISTANCE</Text>
            <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(0)} km
                        </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  map: {
    width,
    height,
  },
  navBar: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    height: 80,
    width,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  navBarText: {
    color: '#19B5FE',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
    paddingTop: 20,
  },
  bottomBar: {
    position: 'absolute',
    height: 60,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.7)',
    width,
    padding: 6,
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  bottomBarGroup: {
    flex: 1,
  },
  buttonDirection: {
    bottom: 72,
    width: 100,
    height: 40,
  },
  bottomBarHeader: {
    color: '#fff',
    fontWeight: '400',
    textAlign: 'center',
  },
  bottomBarContent: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 18,
    marginTop: 5,
    color: '#19B5FE',
    textAlign: 'center',
  },
  buttonStart: {
    flexDirection: 'row',
    alignSelf: 'center',
    bottom: 72,
    width: 100,
    height: 40,
  },
  bubble: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
  loading: {
    alignItems: 'center',
    height: height / 4,
    width: 280,
    paddingVertical: 6,
    backgroundColor: 'mediumseagreen',
    top: 120,
    paddingHorizontal: 12,
    left: 65,
    borderRadius: 54,
  },
});

export default TripInfo;
