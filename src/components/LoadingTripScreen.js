import React, { Component } from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import { Button, Avatar, Icon, Card, Divider } from 'react-native-elements';
import MapView, { Marker, AnimatedRegion, Polyline } from 'react-native-maps';
import moment from 'moment';

const { width, height } = Dimensions.get('window');

export default class LoadingTripScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async handlePress() {
    this.setState({ loading: false });
  }

  render() {
    const { trip, loading, locationToCoords, locationFromCoords } = this.props;
    // come back to * pass in as prop from TypInfo

    console.log('proopy', locationFromCoords, locationToCoords);
    console.log(loading);

    const dateISO = trip.dateISO;
    const countDown = moment(dateISO).fromNow();
    console.log('countdown');
    /* if (loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator size="large" />
        </View>
      );
    } */
    return (
      /* eslint-disable */
			
      <View style={styles.container}>
			<Card title={`Starts in ${countDown}`}>
        <View style={styles.subContainer}>


          <View style={styles.viewStyleTwo}>
			{loading && <ActivityIndicator size="large"/>}
			{!loading && 	<MapView style={{height: height/2.2, width: width*.9}}
									initialRegion={{
										latitude: locationFromCoords.lat,
										longitude: locationFromCoords.lng,
										latitudeDelta: 0.1822,
										longitudeDelta: 0.0821,
									}}
								
									>

		 <MapView.Marker
		 				title="Destination"
            coordinate={{
              latitude: locationToCoords.lat,
              longitude: locationToCoords.lng,
						}}
						pinColor='red'
          />
          <MapView.Marker
            coordinate={{
              latitude: locationFromCoords.lat,
              longitude: locationFromCoords.lng,
						}}
						pinColor='linen'
						title='Origin'
          />
			</MapView>
			}
					
          </View>

					<View style={styles.bottomBar}>
					<Divider style={{ backgroundColor: '#f5f5f0', paddingTop: 2}} />
						<View style={styles.bottomBarContent}>
          		<Avatar size="medium"
											rounded
											icon={{ name: 'drive-eta'}}
										 //	style={{ width: 40, height: 50, borderRadius: 10 }} />
										 />
							<Text style={styles.textDriver}>{trip.owner.name}</Text>
							<Icon
                name="message"
                size={42}
                color="blue"
                onPress={() => console.log('hello')}
                containerStyle={styles.iconMessage}
              />

						</View>
						<Divider style={{ backgroundColor: '#f5f5f0', paddingTop: 2}} />
					</View>

        </View>
			</Card>
      </View>
			
			
				 /* eslint-enable */
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#cecece',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  subContainer: {
    // backgroundColor: '#c9ffb2',
    flex: 1,

    borderRadius: 32,
  },

  countDown: {
    flexDirection: 'row',
    flex: 1,
  },
  location: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  locationFrom: {
    fontSize: 28,
  },
  textLocation: {
    fontSize: 28,
  },
  viewStyleTwo: {
    width: width * 0.95,
    height: height / 2.2,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'orange',
  },
  iconMessage: {
    paddingLeft: 10,
  },
  tripBlur: {
    backgroundColor: '#e5ffde',
    borderRadius: 80,
    width: 180,
    height: 140,
  },
  bottomBar: {
    flex: 1,
    marginTop: 16,
  },
  viewStyleThree: {
    display: 'flex',
    width: 320,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    flexDirection: 'column',
  },
  bottomBarContent: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    // borderColor: '#f5f5f0',
    // borderWidth: 2,
  },
  buttonGroup: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: 12,
  },
  textStyle: {
    textAlign: 'center',
    fontSize: 26,
  },
  textStyleTime: {
    fontSize: 32,
    fontWeight: '700',
  },
  textDriver: {
    color: '#a7ada4',
    fontSize: 24,
    fontWeight: '700',
    textAlign: 'center',
    paddingLeft: 12,
  },
  iconCancel: {
    paddingRight: 32,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: height / 2,
  },
});
