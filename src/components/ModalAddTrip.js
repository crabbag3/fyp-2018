import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Dimensions,
  AsyncStorage,
  Platform,
  ActivityIndicator,
  TouchableHighlight,
  Image,
  Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import Button from 'react-native-button';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import axios from 'axios';
import _ from 'lodash';
import { Icon, Divider } from 'react-native-elements';
import { HOST, apiKey } from '../services/env';

export default class ModalAddTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDateTimePickerVisible: false,
      chosenDate: null,
      dateISO: '',
      spaces: null,
      locationTo: '',
      locationFrom: '',
      locationFromId: '',
      loading: false,
      isVisible: false,
      latitude: '',
      longitude: '',
      predictions: [],
      predictionsTo: [],
      locationToField: false,
      locationFromField: false,
      isSearchLoading: false,
      textVisible: true,
    };
    this.onChangeDestinationDebounced = _.debounce(
      this.onChangeDestination,
      500,
    );
  }

  // functions

  // Live automplete location
  async onChangeDestination(destination) {
    const { coords } = this.props;

    console.log(destination);
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apiKey}
			&input=${destination}&location=
			${coords.latitude},${coords.longitude}&radius=2000`;

    try {
      const result = await fetch(apiUrl);
      const json = await result.json();

      console.log('JSon pred', json);

      this.setState({
        predictions: json.predictions,
        isSearchLoading: false,
        //  predictionsTo: json.predictions,
      });
    } catch (err) {
      console.log(err);
    }
  }

  onChanged(text) {
    let newText = '';
    const numbers = '1234';

    for (let i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText += text[i];
      } else {
        // your call back function
        alert('Please enter a valid entry');
      }
    }
    this.setState({ spaces: newText });
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () =>
    this.setState({ isDateTimePickerVisible: false, textVisible: false });

  _handleDatePicked = datetime => {
    const now = moment();
    if (moment(datetime).isBefore(now)) {
      console.log('isBeforeTikme', moment(datetime).isBefore(now));
      Alert.alert("We don't allow time travellers");
    }

    this._hideDateTimePicker();

    this.setState({
      dateISO: moment(datetime),
      chosenDate: moment(datetime).format('MMMM Do HH:mm'),
      isDateTimePickerVisible: false,
    });
  };

  // API Post a trip call

  handleSubmit = async () => {
    try {
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);

      this.setState({ token: parsed.token, id: parsed.id, owner: parsed.name });
    } catch (error) {
      alert(error);
    }
    const {
      locationFrom,
      locationTo,
      owner,
      chosenDate,
      spaces,
      token,
      id,
      dateISO,
      locationFromId,
      locationToId,
    } = this.state;

    axios
      .post(
        `${HOST}api/users/${id}/trips`,
        {
          location_to: locationTo,
          location_to_id: locationToId,
          location_from: locationFrom,
          location_from_id: locationFromId,
          timeStamp: chosenDate,
          spaces: 0,
          owner,
          dateISO,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then(response => {
        console.log(response);
        this.setState({ loading: false, isVisible: false });
        this.props.makeRemoteRequest();
        alert('Trip posted!');
      })
      .catch(response => {
        console.warn(response);
        this.setState({ loading: false, isVisible: false });

        alert('Error when posting trip!');
      });
  };

  // Modal toggle

  toggleModal = () => {
    this.setState({ isVisible: !this.state.isVisible });
  };

  // OnFocus

  // handle location selected from list of predications
  // variables is name of place and the unique place_id from Google PlacesAPI
  async updateLocation(location, placeId) {
    const { locationFrom, locationTo } = this.state;
    // if locationFrom is selected
    if (this.state.locationFromField) {
      this.setState({
        locationFrom: location,
        locationFromId: placeId,
        predictions: [],
      });

      console.log('Destination ', location, placeId);
    }
    // if locationFrom is selected
    else if (this.state.locationToField) {
      this.setState({
        locationTo: location,
        locationToId: placeId,
        predictions: [],
      });

      console.log('locationTo', location, placeId);
    }
  }

  render() {
    // console.warn('Location', this.state.longitude, this.state.latitude);
    const {
      locationFrom,
      locationTo,

      chosenDate,
      isVisible,
      loading,
    } = this.state;

    // Maps the predications from GooglePlacesApi (LocationTo)
    /* const predictionsTo = this.state.predictionsTo.map(prediction => (
      <TouchableHighlight
        key={prediction.id}
        onPress={() =>
          this.updateLocationTo(
            prediction.structured_formatting.main_text,
            prediction.place_id,
          )
        }
      >
        <View>
          <Text style={styles.suggestions}>
            {' '}
            {prediction.structured_formatting.main_text}{' '}
          </Text>
        </View>
      </TouchableHighlight>
		)); */

    // Maps the predications from GooglePlacesApi
    const predictions = this.state.predictions.map(prediction => (
      <TouchableHighlight
        key={prediction.id}
        onPress={() =>
          this.updateLocation(
            prediction.structured_formatting.main_text,
            prediction.place_id,
          )
        }
      >
        <View>
          <Text style={styles.suggestions}>
            {' '}
            {prediction.structured_formatting.main_text}{' '}
          </Text>
        </View>
      </TouchableHighlight>
    ));

    return (
      <Modal
        onBackdropPress={() => {
          this.toggleModal();
        }}
        isVisible={isVisible}
        hideModalContentWhileAnimating
      >
        <View style={styles.modalViewContainer}>
          <View style={styles.container}>
            <View style={styles.textContainer}>
              <Image
                source={require('../assests/carLogo.png')}
                style={{
                  width: 40,
                  height: 50,
                  borderRadius: 10,
                }}
              />
              <Text style={styles.title}>Post your trip here!</Text>
            </View>

            <TextInput
              style={styles.inputBox}
              underlineColorAndroid="rgba(0,0,0,0)"
              placeholder="Where are you going to?"
              name="locationTo"
              placeholderTextColor="rgba(255,255,255,0.7)"
              blurOnSubmit={false}
              type="text"
              id="locationTo"
              // onBlur={() => this.onBlur()}
              onEndEditing={() => this.setState({ locationToField: false })}
              returnKeyType="next"
              onChangeText={locationTo => {
                this.setState({
                  locationTo,
                  locationToField: true,
                  isSearchLoading: true,
                });
                this.onChangeDestinationDebounced(locationTo);
              }}
              value={locationTo}
            />

            {this.state.isSearchLoading && <ActivityIndicator />}
            {this.state.locationToField && <View>{predictions}</View>}

            <TextInput
              style={styles.inputBox}
              underlineColorAndroid="rgba(0,0,0,0)"
              placeholder="Where are you going from?"
              name="locationFrom"
              placeholderTextColor="rgba(255,255,255,0.7)"
              blurOnSubmit={false}
              type="text"
              id="locationFrom"
              onEndEditing={() => this.setState({ locationFromField: false })}
              returnKeyType="next"
              onChangeText={locationFrom => {
                this.setState({ locationFrom, locationFromField: true });
                this.onChangeDestinationDebounced(locationFrom);
              }}
              value={locationFrom}
            />

            {this.state.locationFromField && <View>{predictions}</View>}
            <TouchableOpacity
              style={styles.inputBox}
              onPress={this._showDateTimePicker}
            >
              {chosenDate === null && (
                <Text
                  isVisible={this.state.textVisible}
                  style={{
                    color: 'rgba(255,255,255,0.7)',
                    fontSize: 16,
                    marginTop: 6,
                  }}
                >
                  When are you going
                </Text>
              )}
              <Text
                style={{
                  color: 'white',
                  fontSize: 16,
                  marginTop: 6,
                }}
              >
                {chosenDate}
              </Text>
            </TouchableOpacity>
            <DateTimePicker
              placeholder="whereaer"
              style={styles.inputBox}
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this._handleDatePicked}
              value={chosenDate}
              onCancel={this._hideDateTimePicker}
              mode="datetime"
            />

            <View style={styles.button}>
              <Button
                style={{ fontSize: 18, color: 'white' }}
                containerStyle={{
                  padding: 8,
                  marginLeft: 70,
                  marginRight: 70,
                  height: 40,
                  borderRadius: 6,
                  backgroundColor: 'mediumseagreen',
                }}
                onPress={this.handleSubmit}
              >
                Save
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 0,
    fontSize: 24,
    fontWeight: '500',
    top: 10,
    paddingBottom: 24,
    paddingHorizontal: 6,
    shadowRadius: 8,
  },
  modalViewContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    height: 360,
    width: width - 480,
    backgroundColor: '#bbcbcb',
    borderColor: '#d6d7da',

    borderRadius: Platform.OS === 'ios' ? 30 : 15,
    shadowRadius: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingTop: 12,
  },
  inputBox: {
    width: 280,
    height: 40,
    backgroundColor: '#82d173',
    borderRadius: 25,
    paddingHorizontal: 32,
    fontSize: 16,
    marginTop: 24,
    color: '#ffffff',
    fontWeight: '400',
  },
  button: {
    flex: 1,
    paddingTop: 12,
  },
  suggestions: {
    backgroundColor: 'white',
    padding: 5,
    fontSize: 16,
    borderWidth: 0.5,
    marginLeft: 5,
    marginRight: 5,
    width: 260,
    height: 37.5,
    borderRadius: 25,
    fontWeight: '400',
  },
});

const { width, height } = Dimensions.get('window');
const CONTAINER_WIDTH = 0.9;
const Modal_HEIGHT = height / 3;
