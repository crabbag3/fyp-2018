import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import LeaderBoards from '../components/LeaderBoards';

export default class Leaderboards extends Component {
  static navigationOptions = {
    title: 'Leaderboards',
    backgroundColor: '#82d172',
    headerStyle: {
      backgroundColor: '#82d172',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <LeaderBoards />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
