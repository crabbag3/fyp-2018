import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import moment from 'react-moment';
import TripInfo from '../components/TripInfo';

export default class TripView extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { item } = this.props.navigation.state.params;

    return (
      <View style={styles.container}>
        <TripInfo navigation={this.props.navigation} trip={item} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
