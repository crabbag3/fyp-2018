import React, { Component } from 'react';
import {
  View,
  AsyncStorage,
  StyleSheet,
  Dimensions,
  Alert,
} from 'react-native';
import {
  Card,
  Button,
  Text,
  Icon,
  Avatar,
  Divider,
} from 'react-native-elements';
import { onSignOut } from '../auth';
import { HOST } from '../services/env';
import { apiFetchUsers } from '../services/api';

export default class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      points: null,
      tripCount: 0,
      token: null,
      data: [],
      id: null,
      ranking: null,
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  // fetch userData from async storage
  makeRemoteRequest = async () => {
    try {
      const user = await AsyncStorage.getItem('userData');
      const parsed = JSON.parse(user);

      this.setState({
        name: parsed.name,
        points: parsed.points,
        tripCount: parsed.completedTripCount,
        token: parsed.token,
        id: parsed.id,
      });
    } catch (error) {
      Alert.alert(error);
    }
    const { token, data, id } = this.state;
    // fetch users that have points
    const url = `${HOST}api/users/`;
    this.setState({ loading: true });
    fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          data: resJson.user,
          loading: false,
        });
        // finds ranking of user
        this.setState({
          ranking: this.state.data.findIndex(item => item._id === id) + 1,
        });
        console.log(this.state.index);
      })
      .catch(error => {
        Alert.alert('Error occured when fetchin data!');
        console.log(error);
        this.setState({ error, loading: false, refreshing: false });
      });
  };
/* eslint-disable */
  renderRanking(ranking) {
    return (
      <View>
        <Text style={styles.textInfo}>#{ranking}</Text>
        <Text style={{ fontSize: 20, fontWeight: '400' }}> Ranked</Text>
      </View>
    );
  }

  render() {
    const { name, points, tripCount, data, id, ranking } = this.state;

    const pointRound = Math.round(points);
    return (
      
      <View>
        <View style={styles.container}>
          <Card featuredTitleStyle={styles.cardTitle} title={name}>
            <View style={styles.cardContainer}>

              <View style={styles.textContainer}>
              <View style={styles.infoContainers}>
							
							<View style={styles.info1}>
							<Text style={styles.textInfo}>{pointRound}</Text>
							<Text style={styles.textInfoSub}>Points</Text>
							</View>
							<View style={styles.info2}>
							<Text style={styles.textInfo}>{tripCount}</Text>
							<Text style={styles.textInfoSub}>Trips</Text>
							</View>
						
							<View style={styles.info3}>
							{ranking ===0 && 	<Text style={styles.textInfoSubSub}>Earn points to be ranked</Text>}
							{ranking != 0 && this.renderRanking(ranking)}
							
							<View style={styles.divider}>
							<Divider style={{ backgroundColor: '#f5f5f0', justifyContent: 'center'  }} />
							</View>
							</View>
				
							</View>
							<View style={styles.divider}>
							<Divider style={{ backgroundColor: '#f5f5f0',}} />
							<Divider style={{ backgroundColor: '#f5f5f0',  }} />
							</View>
							

							<View style={styles.bottomContainer}>

							<View style={styles.info1}>
							<Text style={styles.textInfo}>FACT</Text>
							<Text style={styles.textInfoSub}></Text>
							</View>
							<View style={styles.info2}>
							<Text style={styles.textInfo}>FACT</Text>
							<Text style={styles.textInfoSub}></Text>
							</View>
							<View style={styles.info3}>
							<Text style={styles.textInfo}>FACT</Text>
							<Text style={styles.textInfoSub}></Text>
							</View>

							</View>
              </View>
              <View style={styles.paddingButton}>
						
							</View>

              <Button
                backgroundColor="#03A9F4"
                title="SIGN OUT"
                onPress={() =>
                  onSignOut().then(() =>
                    this.props.navigation.navigate('SignedOut'),
                  )
                }
              />
            </View>
          </Card>
        </View>
      </View>
    );
  }
}
/* eslint-enable */

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    //  backgroundColor: 'black',
  },

  textContainer: {
    display: 'flex',
    backgroundColor: '#e5ffde',
    borderRadius: 10,
    height: 360,
  },
  infoContainers: {
    height: 180,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomContainer: {
    height: 180,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  cardTitle: {
    fontSize: 24,
  },
  divider: {},
  info1: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: 80,
    height: 90,
    backgroundColor: '#82d172',
    borderRadius: 24,
  },
  info2: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: 80,
    height: 90,
    backgroundColor: '#82d172',
    borderRadius: 24,
  },
  info3: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: 80,
    height: 90,
    backgroundColor: '#82d172',
    borderRadius: 24,
  },
  textInfo: {
    fontSize: 26,
    fontWeight: '700',
    textShadowRadius: 4,
    textShadowColor: '#f5f5f0',
    textShadowOffset: { width: 2, height: 2 },
  },
  textInfoSub: {
    fontSize: 20,
    fontWeight: '400',
  },
  textInfoSubSub: {
    fontSize: 16,
    fontWeight: '400',
    paddingLeft: 12,
  },
  textPre: {
    marginTop: 10,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  textPoints: {
    marginTop: 10,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    fontSize: 32,
  },
  paddingButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 80,
    height: 380,
    borderRadius: 40,
    alignSelf: 'center',
    marginBottom: 20,
  },
});
