import React, { Component } from 'react';
import { View, Text, Button, TouchableOpacity, StyleSheet } from 'react-native';

export default class Welcome extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
        <Text> Welcome to Carshare </Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('HomeTabNavigator')}
        >
          <Text style={styles.signupButton}> Dismiss</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  signupButton: {
    color: 'black',
    fontSize: 32,
    fontWeight: '500',
  },
});
