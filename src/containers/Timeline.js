import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ListActiveTrips from '../components/ListActiveTrips';

export default class Timeline extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
        <ListActiveTrips navigation={this.props.navigation} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
