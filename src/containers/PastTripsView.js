import React, { Component } from 'react';
import { View, Text } from 'react-native';
import PastTrips from '../components/PastTrips';

export default class PastTripView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <PastTrips />
      </View>
    );
  }
}
