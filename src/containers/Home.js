import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Button from 'react-native-elements';
import Logo from '../components/Logo';
import ListTrips from '../components/ListTrips';

export default class Home extends Component {
  static navigationOptions = {
    title: 'Find a Trip',
    backgroundColor: '#82d172',
    headerStyle: {
      backgroundColor: '#82d172',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      latitude: null,
      longitude: null,
      error: null,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <ListTrips navigation={this.props.navigation} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    justifyContent: 'flex-end',
  },
  box: {
    flex: 1,
    borderColor: '#d6d7da',
    alignItems: 'center',
    backgroundColor: '#455a64',
  },
  form: {
    flex: 7,
    alignItems: 'center',
  },
  titles: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: 0,
    paddingVertical: 5,
  },
  title: {
    flex: 1,
    justifyContent: 'center',
    color: '#cc9d8e',
    fontSize: 32,
    fontWeight: 'bold',
    paddingTop: 8,

    height: 24,
  },
});
