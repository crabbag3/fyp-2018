import axios from 'axios';
import AsyncStorage from 'react-native';
import { HOST } from './env';

export function apiCall(method, path, data) {
  return new Promise((resolve, reject) =>
    axios[method](path, data)
      .then(res => resolve(res.data))
      .catch(err => reject(err.response.data.error)),
  );
}
// Fetch user date - stored in async storage
export async function userData() {
  try {
    const user = await AsyncStorage.getItem('userData');
    const parsed = JSON.parse(user);
    return parsed;
  } catch (error) {
    return error;
  }
}

// POST Trip
export const handleSubmit = () => {
  axios
    .post(
      `${HOST}api/users/5bf2a01752540e0015b43268/trips`,
      {
        location_to: this.prop.locationTo,
        location_from: this.prop.locationFrom,
        timeStamp: this.state.chosenDate,
        spaces: this.state.spaces,
      },
      {
        headers: {
          Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViZjJhMDE3NTI1NDBlMDAxNWI0MzI2OCIsImVtYWlsIjoib3NjQGdtYWlsLmNvbSIsImlhdCI6MTU0MjgxMzE2N30.9kXGmjVsOKptILiqy6PAgrX2uyH8k3lLZ2t6s1ZprHo`,
        },
      },
    )
    .then(response => {
      console.log(response);

      alert('Trip posted!');
    })
    .catch(response => {
      console.warn(response);

      alert('Error occured!');
    });
};

// Trip Finish (Single User) - Update Trip
export async function apiTripFinish(
  tripScore,
  distanceTravelled,
  trip,
  token,
  id,
  ownerTrue,
  userTrue,
) {
  Math.round(distanceTravelled);
  console.log('tripFinito', distanceTravelled, tripScore);
  axios
    .put(
      `${HOST}api/users/${id}/trips/${trip._id}/trip-complete-user`,
      {
        distanceTravelled,
        tripScore,
        ownerTrue,
        userTrue,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )
    .then(response => {
      console.log('api sucess', response);
      // stops watching user position
      navigator.geolocation.clearWatch(this.watchID);
      /* this.props.navigation.navigate('TripFinish', {
        distanceTravelled,
        trip,
        tripScore,
      }); */
    })
    .catch(error => {
      console.log(error);
    });
}

// Trip finish - when both users have completed the trip
export async function apiTripTotalFinish(trip, token, id) {
  axios
    .put(
      `${HOST}api/users/${id}/trips/${trip._id}/tripComplete`,
      {
        tripScore: trip.tripScore,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )
    .then(response => {
      console.log('success', response);
      // stops watching user position

      /* this.props.navigation.navigate('TripFinish', {
        distanceTravelled,
        trip,
        tripScore,
      }); */
    })
    .catch(error => {
      console.log(error);
    });
}

// Fetch Trips - multiple routes use ${type} to specifiy
// come back to not
export async function apiTripFetch(type, id, token) {
  const url = `${HOST}api/users/${id}/trips/${type}`;
  this.setState({ loading: true });
  fetch(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
    .then(res => res.json())
    .then(resJson => {
      this.setState({
        data: resJson,
        error: resJson.error || null,
        loading: false,
      });
      this.arrayholder = resJson;
    })
    .catch(error => {
      alert('Error occured when fetchin data!');
      this.setState({ error, loading: false, refreshing: false });
    });
}

// Get Single Trip
// come back to not
export async function apiTripFetchSingle(id, token, trip_id) {
  const url = `${HOST}api/users/${id}/trips/${trip_id}`;
  this.setState({ loading: true });
  fetch(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
    .then(res => res.json())
    .then(resJson => {
      this.setState({
        data: resJson,
        error: resJson.error || null,
        loading: false,
      });
      this.arrayholder = resJson;
    })
    .catch(error => {
      alert('Error occured when fetchin data!');
      this.setState({ error, loading: false, refreshing: false });
    });
}

// Fetch list of user (Trip count > 1)
// To display in leadboars
export async function apiFetchUsers(token) {
  try {
    const url = `${HOST}api/users`;
    const result = await fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const json = await result.json();

    return json;
  } catch (err) {
    return err;
  }
}
