import Polyline from '@mapbox/polyline';
import { apiKey } from './env'; // 'walking';

const mode = 'driving';

export async function handleDirections(origin, destination) {
  const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${apiKey}&mode=${mode}`;
  try {
    console.log(origin, destination);
    const resp = await fetch(url);
    const respJson = await resp.json();
    console.log(respJson);
    const points = Polyline.decode(respJson.routes[0].overview_polyline.points);
    const coords = points.map((point, index) => ({
      latitude: point[0],
      longitude: point[1],
    }));

    console.log('success');
    return coords;
  } catch (error) {
    console.log(error);
    return error;
  }
}

export async function handleTripCoords(place_id) {
  const apiLocationFrom = `https://maps.googleapis.com/maps/api/place/details/json?key=${apiKey}
			&placeid=${place_id}`;

  // Returns place object info
  try {
    const result = await fetch(apiLocationFrom);
    const json = await result.json();
    // console.log('To', json.result.geometry.location);
    return json.result.geometry.location;
  } catch (error) {
    return error;
  }
}
