import { AsyncStorage } from 'react-native';

// Not used
//  export const onSignIn = async () => AsyncStorage.setItem('userData', 'true');

export const onSignOut = () => AsyncStorage.removeItem('userData');

export const isSignedIn = () =>
  new Promise((resolve, reject) => {
    AsyncStorage.getItem('userData')
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
//
