import React from 'react';
import { Platform, StatusBar } from 'react-native';
import {
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
  createMaterialTopTabNavigator,
} from 'react-navigation';

import { Icon } from 'react-native-elements';
import Welcome from './containers/Welcome';
import Timeline from './containers/Timeline';
import Login from './containers/Login';
import Signup from './containers/Signup';
import Home from './containers/Home';
import Settings from './containers/Settings';
import SelectedTrip from './components/SelectedTrip';
import TripView from './containers/TripView';
import PastTripView from './containers/PastTripsView';
import TripFinish from './components/TripFinish';
import Leaderboards from './containers/Leaderboards';

// Navigation for users signed out
export const SignedOut = createStackNavigator({
  SignUp: { screen: Signup },
  SignIn: { screen: Login },
});

// Finished Trip

// Navigation for Trips(Feed)
export const MyTripsStack = createStackNavigator({
  Timeline,
  TripView,
});

export const MainTripStack = createStackNavigator(
  {
    MyTripsStack,
    TripFinish,
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

// Home Stack
export const HomeView = createStackNavigator({
  Home,
});

// Leaderboards Stack
export const LeaderboardsView = createStackNavigator({
  Leaderboards,
});

// create navigation for current and past trips
export const statusTrip = createMaterialTopTabNavigator({
  'Active Trips': {
    screen: MainTripStack,
    navigationOptions: {
      tabBarOptions: {
        labelStyle: {
          fontSize: 12,
        },

        style: {
          backgroundColor: '#82d172',
        },
      },
    },
  },
  'Past Trips': {
    screen: PastTripView,
    navigationOptions: {
      tabBarOptions: {
        labelStyle: {
          fontSize: 12,
        },

        style: {
          backgroundColor: '#82d172',
        },
      },
    },
  },
});

// Navigation for users signed in
export const SignedIn = createBottomTabNavigator(
  {
    Timeline: {
      screen: statusTrip,
      navigationOptions: {
        title: 'First Page',
        // Sets Header text of Status Bar
        headerStyle: {
          backgroundColor: '#82d172',
          // Sets Header color
        },
        headerTintColor: '#fff',
        // Sets Header text color
        headerTitleStyle: {
          fontWeight: 'bold',
          // Sets Header text style
        },
        tabBarLabel: 'My Trips',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="list" size={32} color={tintColor} />
        ),
      },
    },

    Home: {
      screen: HomeView,
      navigationOptions: {
        title: 'Find aasd Trip',
        tabBarLabel: 'Home',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" size={32} color={tintColor} />
        ),
      },
    },
    Leaderboards: {
      screen: LeaderboardsView,
      navigationOptions: {
        tabBarLabel: 'Leaderboards',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="people" size={32} color={tintColor} />
        ),
      },
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="account-circle" size={32} color={tintColor} />
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
  },
);

// Default route
export const createRootNavigator = (signedIn = false) =>
  createSwitchNavigator(
    {
      SignedIn: {
        screen: SignedIn,
      },
      SignedOut: {
        screen: SignedOut,
      },
    },

    {
      initialRouteName: signedIn ? 'SignedIn' : 'SignedOut',
    },
  );
