import React from 'react';

import { AsyncStorage } from 'react-native';
import { createRootNavigator, SignedOut, SignedIn } from './router';
import { isSignedIn } from './auth';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: false,
      latitude: null,
      longitude: null,
    };
  }

  // Checks if user is signed in then display appropiriate route
  componentDidMount() {
    /* navigator.geolocation.getCurrentPosition(
      position => {
        console.log(position);
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
        const positionCords = {
          latitude: this.state.latitude,
          longitude: this.state.longitude,
        };

        console.log('LOCATION', positionCords);
        AsyncStorage.setItem('userLocation', JSON.stringify(positionCords));
      },
      error => {
        console.log(error);
      },

      { enableHighAccuracy: true, timeout: 5000 },
    ); */

    isSignedIn()
      .then(res => this.setState({ signedIn: res, checkedSignIn: true }))
      .catch(err => alert('An error occurred'));
  }

  render() {
    const { checkedSignIn, signedIn } = this.state;

    // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
    if (!checkedSignIn) {
      return null;
    }

    const Layout = createRootNavigator(signedIn);
    return <Layout />;
  }
}
