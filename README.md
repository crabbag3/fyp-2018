# CarShare
 
## Mobile application that encourages use through the proccess of gamification.
 
* Find/Post your Trip
* Earn points for completed trips
* Geolocation - Places,Directions and Trip Validation
* Leaderboards

## API
https://gitlab.com/crabbag3/fyp_server